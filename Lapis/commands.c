
/*****************************************************************************
 *
 *
 *
 *
 *
 ****************************************************************************/

#include "UARTconsole.h"
#include "BLE113comm.h"
#include "cmd_def.h"
void ble_default(const void*v)
{
}

void ble_rsp_system_reset(const void* nul)
{
}

void ble_rsp_system_hello(const void* nul)
{
	UARTconsole_printf("Hello!\n");
#ifdef DEBUG
	UARTconsole_printf("DEBUG MODE\n\n");
#endif
}

void ble_rsp_system_address_get(const struct ble_msg_system_address_get_rsp_t *msg)
{
	BLE113comm_addresses->local = msg->address;
#ifdef DEBUG
		UARTconsole_printf("Local address: %x:%x:%x:%x:%x:%x\n",
            BLE113comm_addresses->local.addr[5],
			BLE113comm_addresses->local.addr[4],
			BLE113comm_addresses->local.addr[3],
			BLE113comm_addresses->local.addr[2],
			BLE113comm_addresses->local.addr[1],
			BLE113comm_addresses->local.addr[0]);
#endif
}

void ble_rsp_system_reg_write(const struct ble_msg_system_reg_write_rsp_t *msg)
{
}

void ble_rsp_system_reg_read(const struct ble_msg_system_reg_read_rsp_t *msg)
{
}

void ble_rsp_system_get_counters(const struct ble_msg_system_get_counters_rsp_t *msg)
{
}

void ble_rsp_system_get_connections(const struct ble_msg_system_get_connections_rsp_t *msg)
{
}

void ble_rsp_system_read_memory(const struct ble_msg_system_read_memory_rsp_t *msg)
{
}

void ble_rsp_system_get_info(const struct ble_msg_system_get_info_rsp_t *msg)
{
}

void ble_rsp_system_endpoint_tx(const struct ble_msg_system_endpoint_tx_rsp_t *msg)
{
}

void ble_rsp_system_whitelist_append(const struct ble_msg_system_whitelist_append_rsp_t *msg)
{
}

void ble_rsp_system_whitelist_remove(const struct ble_msg_system_whitelist_remove_rsp_t *msg)
{
}

void ble_rsp_system_whitelist_clear(const void* nul)
{
}

void ble_rsp_system_endpoint_rx(const struct ble_msg_system_endpoint_rx_rsp_t *msg)
{
}

void ble_rsp_system_endpoint_set_watermarks(const struct ble_msg_system_endpoint_set_watermarks_rsp_t *msg)
{
}

void ble_rsp_system_aes_setkey(const void* nul)
{
}

void ble_rsp_system_aes_encrypt(const struct ble_msg_system_aes_encrypt_rsp_t *msg)
{
}

void ble_rsp_system_aes_decrypt(const struct ble_msg_system_aes_decrypt_rsp_t *msg)
{
}

void ble_rsp_flash_ps_defrag(const void* nul)
{
}

void ble_rsp_flash_ps_dump(const void* nul)
{
}

void ble_rsp_flash_ps_erase_all(const void* nul)
{
}

void ble_rsp_flash_ps_save(const struct ble_msg_flash_ps_save_rsp_t *msg)
{
}

void ble_rsp_flash_ps_load(const struct ble_msg_flash_ps_load_rsp_t *msg)
{
}

void ble_rsp_flash_ps_erase(const void* nul)
{
}

void ble_rsp_flash_erase_page(const struct ble_msg_flash_erase_page_rsp_t *msg)
{
}

void ble_rsp_flash_write_data(const struct ble_msg_flash_write_data_rsp_t *msg)
{
}

void ble_rsp_flash_read_data(const struct ble_msg_flash_read_data_rsp_t *msg)
{
}

void ble_rsp_attributes_write(const struct ble_msg_attributes_write_rsp_t *msg)
{
}

void ble_rsp_attributes_read(const struct ble_msg_attributes_read_rsp_t *msg)
{
}

void ble_rsp_attributes_read_type(const struct ble_msg_attributes_read_type_rsp_t *msg)
{
}

void ble_rsp_attributes_user_read_response(const void* nul)
{
}

void ble_rsp_attributes_user_write_response(const void* nul)
{
}

void ble_rsp_attributes_send(const struct ble_msg_attributes_send_rsp_t *msg)
{
}

void ble_rsp_connection_disconnect(const struct ble_msg_connection_disconnect_rsp_t *msg)
{
	UARTconsole_printf("<--Disconnecting handle: 0x%x ...\n", msg->connection);
}

void ble_rsp_connection_get_rssi(const struct ble_msg_connection_get_rssi_rsp_t *msg)
{
	UARTconsole_printf("Handle: %x", msg->connection);
	UARTconsole_printf("RSSI: %i\n", msg->rssi);
}

void ble_rsp_connection_update(const struct ble_msg_connection_update_rsp_t *msg)
{
}

void ble_rsp_connection_version_update(const struct ble_msg_connection_version_update_rsp_t *msg)
{
}

void ble_rsp_connection_channel_map_get(const struct ble_msg_connection_channel_map_get_rsp_t *msg)
{
}

void ble_rsp_connection_channel_map_set(const struct ble_msg_connection_channel_map_set_rsp_t *msg)
{
}

void ble_rsp_connection_features_get(const struct ble_msg_connection_features_get_rsp_t *msg)
{
}

void ble_rsp_connection_get_status(const struct ble_msg_connection_get_status_rsp_t *msg)
{
	UARTconsole_printf("Getting connection status...\n");

}

void ble_rsp_connection_raw_tx(const struct ble_msg_connection_raw_tx_rsp_t *msg)
{
}

void ble_rsp_attclient_find_by_type_value(const struct ble_msg_attclient_find_by_type_value_rsp_t *msg)
{
}

void ble_rsp_attclient_read_by_group_type(const struct ble_msg_attclient_read_by_group_type_rsp_t *msg)
{
	UARTconsole_printf("Reading Group Type...\n");
}

void ble_rsp_attclient_read_by_type(const struct ble_msg_attclient_read_by_type_rsp_t *msg)
{
}

void ble_rsp_attclient_find_information(const struct ble_msg_attclient_find_information_rsp_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Response: Att client find information\n");
	UARTconsole_printf("Finding information...\n");
	UARTconsole_printf("\tHandle: %x\n", msg->connection);
#endif
	BLE113comm_errors(msg->result);
}

void ble_rsp_attclient_read_by_handle(const struct ble_msg_attclient_read_by_handle_rsp_t *msg)
{
}

void ble_rsp_attclient_attribute_write(const struct ble_msg_attclient_attribute_write_rsp_t *msg)
{
}

void ble_rsp_attclient_write_command(const struct ble_msg_attclient_write_command_rsp_t *msg)
{
}

void ble_rsp_attclient_indicate_confirm(const struct ble_msg_attclient_indicate_confirm_rsp_t *msg)
{
}

void ble_rsp_attclient_read_long(const struct ble_msg_attclient_read_long_rsp_t *msg)
{
}

void ble_rsp_attclient_prepare_write(const struct ble_msg_attclient_prepare_write_rsp_t *msg)
{
}

void ble_rsp_attclient_execute_write(const struct ble_msg_attclient_execute_write_rsp_t *msg)
{
}

void ble_rsp_attclient_read_multiple(const struct ble_msg_attclient_read_multiple_rsp_t *msg)
{
}

void ble_rsp_sm_encrypt_start(const struct ble_msg_sm_encrypt_start_rsp_t *msg)
{
}

void ble_rsp_sm_set_bondable_mode(const void* nul)
{
}

void ble_rsp_sm_delete_bonding(const struct ble_msg_sm_delete_bonding_rsp_t *msg)
{
}

void ble_rsp_sm_set_parameters(const void* nul)
{
}

void ble_rsp_sm_passkey_entry(const struct ble_msg_sm_passkey_entry_rsp_t *msg)
{
}

void ble_rsp_sm_get_bonds(const struct ble_msg_sm_get_bonds_rsp_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Get bonds response\n"
			"Number of currently bonded devices: %x\n", msg->bonds);
#endif
}

void ble_rsp_sm_set_oob_data(const void* nul)
{
}

void ble_rsp_sm_whitelist_bonds(const struct ble_msg_sm_whitelist_bonds_rsp_t *msg)
{
}

void ble_rsp_gap_set_privacy_flags(const void* nul)
{
}

void ble_rsp_gap_set_mode(const struct ble_msg_gap_set_mode_rsp_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Mode set\n");
	BLE113comm_errors(msg->result);
#endif

}

void ble_rsp_gap_discover(const struct ble_msg_gap_discover_rsp_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Scanning...\n");
	BLE113comm_errors(msg->result);
#endif
}

void ble_rsp_gap_connect_direct(const struct ble_msg_gap_connect_direct_rsp_t *msg)
{
}

void ble_rsp_gap_end_procedure(const struct ble_msg_gap_end_procedure_rsp_t *msg)
{
}

void ble_rsp_gap_connect_selective(const struct ble_msg_gap_connect_selective_rsp_t *msg)
{
}

void ble_rsp_gap_set_filtering(const struct ble_msg_gap_set_filtering_rsp_t *msg)
{
}

void ble_rsp_gap_set_scan_parameters(const struct ble_msg_gap_set_scan_parameters_rsp_t *msg)
{
}

void ble_rsp_gap_set_adv_parameters(const struct ble_msg_gap_set_adv_parameters_rsp_t *msg)
{
}

void ble_rsp_gap_set_adv_data(const struct ble_msg_gap_set_adv_data_rsp_t *msg)
{
}

void ble_rsp_gap_set_directed_connectable_mode(const struct ble_msg_gap_set_directed_connectable_mode_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_config_irq(const struct ble_msg_hardware_io_port_config_irq_rsp_t *msg)
{
}

void ble_rsp_hardware_set_soft_timer(const struct ble_msg_hardware_set_soft_timer_rsp_t *msg)
{
}

void ble_rsp_hardware_adc_read(const struct ble_msg_hardware_adc_read_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_config_direction(const struct ble_msg_hardware_io_port_config_direction_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_config_function(const struct ble_msg_hardware_io_port_config_function_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_config_pull(const struct ble_msg_hardware_io_port_config_pull_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_write(const struct ble_msg_hardware_io_port_write_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_read(const struct ble_msg_hardware_io_port_read_rsp_t *msg)
{
}

void ble_rsp_hardware_spi_config(const struct ble_msg_hardware_spi_config_rsp_t *msg)
{
}

void ble_rsp_hardware_spi_transfer(const struct ble_msg_hardware_spi_transfer_rsp_t *msg)
{
}

void ble_rsp_hardware_i2c_read(const struct ble_msg_hardware_i2c_read_rsp_t *msg)
{
}

void ble_rsp_hardware_i2c_write(const struct ble_msg_hardware_i2c_write_rsp_t *msg)
{
}

void ble_rsp_hardware_set_txpower(const void* nul)
{
}

void ble_rsp_hardware_timer_comparator(const struct ble_msg_hardware_timer_comparator_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_irq_enable(const struct ble_msg_hardware_io_port_irq_enable_rsp_t *msg)
{
}

void ble_rsp_hardware_io_port_irq_direction(const struct ble_msg_hardware_io_port_irq_direction_rsp_t *msg)
{
}

void ble_rsp_hardware_analog_comparator_enable(const void* nul)
{
}

void ble_rsp_hardware_analog_comparator_read(const struct ble_msg_hardware_analog_comparator_read_rsp_t *msg)
{
}

void ble_rsp_hardware_analog_comparator_config_irq(const struct ble_msg_hardware_analog_comparator_config_irq_rsp_t *msg)
{
}

void ble_rsp_hardware_set_rxgain(const void* nul)
{
}

void ble_rsp_hardware_usb_enable(const struct ble_msg_hardware_usb_enable_rsp_t *msg)
{
}

void ble_rsp_test_phy_tx(const void* nul)
{
}

void ble_rsp_test_phy_rx(const void* nul)
{
}

void ble_rsp_test_phy_end(const struct ble_msg_test_phy_end_rsp_t *msg)
{
}

void ble_rsp_test_phy_reset(const void* nul)
{
}

void ble_rsp_test_get_channel_map(const struct ble_msg_test_get_channel_map_rsp_t *msg)
{
}

void ble_rsp_test_debug(const struct ble_msg_test_debug_rsp_t *msg)
{
}

void ble_rsp_test_channel_mode(const void* nul)
{
}

void ble_rsp_dfu_reset(const void* nul)
{
}

void ble_rsp_dfu_flash_set_address(const struct ble_msg_dfu_flash_set_address_rsp_t *msg)
{
}

void ble_rsp_dfu_flash_upload(const struct ble_msg_dfu_flash_upload_rsp_t *msg)
{
}

void ble_rsp_dfu_flash_upload_finish(const struct ble_msg_dfu_flash_upload_finish_rsp_t *msg)
{
}

void ble_evt_system_boot(const struct ble_msg_system_boot_evt_t *msg)
{
	UARTconsole_printf("Boot event\n");

	UARTconsole_printf("Major: %02d \nMinor: %02d \nPatch: %02d \nBuild: %02d \nll_version: %02d \nProtocol: %02d \nHW: %02d\n\n",
			msg->major,
			msg->minor,
			msg->patch,
			msg->build,
			msg->ll_version,
			msg->protocol_version,
			msg->hw);
}

void ble_evt_system_debug(const struct ble_msg_system_debug_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("System Debug Event\n");
#endif
}

void ble_evt_system_endpoint_watermark_rx(const struct ble_msg_system_endpoint_watermark_rx_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("System endpoint watermark RX event\n");
#endif
}

void ble_evt_system_endpoint_watermark_tx(const struct ble_msg_system_endpoint_watermark_tx_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("System endpoint watermark TX event\n");
#endif

}

void ble_evt_system_script_failure(const struct ble_msg_system_script_failure_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("System script failure event\n");
#endif
}

void ble_evt_system_no_license_key(const void* nul)
{
#ifdef DEBUG
	UARTconsole_printf("System no license key event\n");
#endif
}

void ble_evt_system_protocol_error(const struct ble_msg_system_protocol_error_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("System protocol error event\n");
	BLE113comm_errors(msg->reason);
#endif

}

void ble_evt_flash_ps_key(const struct ble_msg_flash_ps_key_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Flash ps key event\n");
#endif
}

void ble_evt_attributes_value(const struct ble_msg_attributes_value_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attributes value event\n");
#endif
}

void ble_evt_attributes_user_read_request(const struct ble_msg_attributes_user_read_request_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attributes user read request event\n");
#endif
}

void ble_evt_attributes_status(const struct ble_msg_attributes_status_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attributes status event\n");
#endif
}

void ble_evt_connection_status(const struct ble_msg_connection_status_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Connection status event\n");
	UARTconsole_printf("\tFlags: %x\n", msg->flags);
#endif

	BLE113comm_connection->connection = msg->connection;		// Connection handle
	BLE113comm_connection->flags = msg->flags;					// Connection status flags use connstatus-enumerator
	BLE113comm_connection->address = msg->address;				// Remote devices Bluetooth address
	BLE113comm_connection->address_type = msg->address_type;	// Remote address type, see: Bluetooth Address Types--gap
	BLE113comm_connection->conn_interval = msg->conn_interval;	// Current connection interval (units of 1.25 ms)
	BLE113comm_connection->timeout = msg->timeout;				// Current supervision timeout (units of 10 ms)
	BLE113comm_connection->latency = msg->latency;				// Slave latency which tells how many connection intervals the slave may skip
	BLE113comm_connection->bonding = msg->bonding;				// Bonding handle if the device has been bonded with. Otherwise: 0xFF

	if(msg->flags & connection_connected)	// This status flag tells the connection exists to a remote device
	{
		UARTconsole_printf("Connected handle: %x\n", msg->connection);
		UARTconsole_printf("Bonding: %x\n", msg->bonding);
//		BLE113comm_set_state(state_connected);
		BLE113comm_set_state(state_finding_services);
	}
	if(msg->flags & connection_completed)	// Connection completed flag, which is used to tell a new connection has been created.
	{
		UARTconsole_printf("New connection established.\n");
		BLE113comm_set_state(state_finding_services);
	}
}

void ble_evt_connection_version_ind(const struct ble_msg_connection_version_ind_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Connection version indicate event\n");
	UARTconsole_printf("\tConnection handle: %x\n"
			"\tBT controller specification ver: %x\n"
			"\tManufacturer ID: %x\n"
			"\tBT controller sub ver: %x\n",
			msg->connection, msg->vers_nr, msg->comp_id, msg->sub_vers_nr);
#endif
}

void ble_evt_connection_feature_ind(const struct ble_msg_connection_feature_ind_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Connection feature indicate event\n");
#endif
}

void ble_evt_connection_raw_rx(const struct ble_msg_connection_raw_rx_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Connection raw RX event\n");
#endif
}

void ble_evt_connection_disconnected(const struct ble_msg_connection_disconnected_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("<--Disconnected handle: %x\n", msg->connection);
	BLE113comm_errors(msg->reason);
#endif
	BLE113comm_set_state(state_disconnected);
	ble_cmd_gap_set_mode(gap_general_discoverable, gap_undirected_connectable);
}

void ble_evt_attclient_indicated(const struct ble_msg_attclient_indicated_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attribute client indicated event\n");
#endif
}

void ble_evt_attclient_procedure_completed(const struct ble_msg_attclient_procedure_completed_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attribute Client procedure completed event\n");
	UARTconsole_printf("Handle: %x\n", msg->connection);
	UARTconsole_printf("Characteristic handle: %x\n", msg->chrhandle);
	BLE113comm_errors(msg->result);
#endif
	if(BLE113comm_get_state() == state_finding_services)
	{
		BLE113comm_set_state(state_finding_attributes);
	}
}

void ble_evt_attclient_group_found(const struct ble_msg_attclient_group_found_evt_t *msg)
{
    uint16 uuid = (msg->uuid.data[1] << 8) | msg->uuid.data[0];
#ifdef DEBUG
	UARTconsole_printf("Attribute Client group found event\n");
	UARTconsole_printf("\tHandle: %x\n",msg->connection);
	UARTconsole_printf("\tStart: %x\n", msg->start);
	UARTconsole_printf("\tEnd: %x\n", msg->end);
	UARTconsole_printf("\tLength: %x\n", msg->uuid.len);
	UARTconsole_printf("\tUUID: %x\n", uuid);
#endif

	if(BLE113comm_get_state() == state_finding_services && uuid == GENERIC_ACCESS_UUID)
	{
		BLE113comm_services->generic_service_handle_start = msg->start;
		BLE113comm_services->generic_service_handle_end = msg->end;
	}
}

void ble_evt_attclient_attribute_found(const struct ble_msg_attclient_attribute_found_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attribute Client attribute found event\n");
#endif
}

void ble_evt_attclient_find_information_found(const struct ble_msg_attclient_find_information_found_evt_t *msg)
{
	uint16 uuid = msg->uuid.data[1]<<8 | msg->uuid.data[0];
#ifdef DEBUG
	UARTconsole_printf("Attribute client find information event\n");
	UARTconsole_printf("\tHandle: %x\n", msg->connection);
	UARTconsole_printf("\tLength: %x\n", msg->uuid.len);
	UARTconsole_printf("\tUUID: %x\n", uuid);
	UARTconsole_printf("\tCharacteristic handle: %x\n", msg->chrhandle);
#endif
	if(uuid == REMOTE_DEVICE_NAME)
	{
		UARTconsole_printf("\tDevice name: %s\n", msg->uuid);
	}
}

void ble_evt_attclient_attribute_value(const struct ble_msg_attclient_attribute_value_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attribute client attribute value event\n");
#endif
}

void ble_evt_attclient_read_multiple_response(const struct ble_msg_attclient_read_multiple_response_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Attribute client read multiple resp event\n");
#endif
}

void ble_evt_sm_smp_data(const struct ble_msg_sm_smp_data_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Security Manager smp data event\n");
#endif
}

void ble_evt_sm_bonding_fail(const struct ble_msg_sm_bonding_fail_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Security Manager bonding fail event\n");
	UARTconsole_printf("Handle: %x\n", msg->handle);
	BLE113comm_errors(msg->result);
#endif
}

void ble_evt_sm_passkey_display(const struct ble_msg_sm_passkey_display_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Security Manager passkey dispay event\n");
	UARTconsole_printf("Handle: %x\n",msg->handle);
#endif
}

void ble_evt_sm_passkey_request(const struct ble_msg_sm_passkey_request_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Security Manager passkey request event\n");
#endif
}

void ble_evt_sm_bond_status(const struct ble_msg_sm_bond_status_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Security Manager bond status event\n");
	UARTconsole_printf("Bond handle: %x\n"
			"Encryption Keysize: %x\n"
			"Man-in-the-middle used: %x\n"
			"Keys stored for bonding: %x\n",
			msg->bond, msg->keysize, msg->mitm, msg->keys);
#endif
}

void ble_evt_gap_scan_response(const struct ble_msg_gap_scan_response_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("GAP scan response event\n");
#endif
//	BLE113comm_connection->connection = msg->connection;			// Connection handle
//	BLE113comm_connection->flags = msg->flags;					// Connection status flags use connstatus-enumerator
//	BLE113comm_connection->address = msg->address;				// Remote devices Bluetooth address
//	BLE113comm_connection->address_type = msg->address_type;		// Remote address type, see: Bluetooth Address Types--gap
//	BLE113comm_connection->conn_interval = msg->conn_interval;	// Current connection interval (units of 1.25 ms)
//	BLE113comm_connection->timeout = msg->timeout;				// Current supervision timeout (units of 10 ms)
//	BLE113comm_connection->latency = msg->latency;				// Slave latency which tells how many connection intervals the slave may skip
//	BLE113comm_connection->bonding = msg->bonding;				// Bonding handle if the device has been bonded with. Otherwise: 0xFF

	UARTconsole_printf("RSSI: %d\n", (int8)msg->rssi);
}

void ble_evt_gap_mode_changed(const struct ble_msg_gap_mode_changed_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("GAP mode changed event\n");
#endif
}

void ble_evt_hardware_io_port_status(const struct ble_msg_hardware_io_port_status_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("GAP hardware IO port event\n");
#endif
}

void ble_evt_hardware_soft_timer(const struct ble_msg_hardware_soft_timer_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Hardware soft timer event\n");
#endif
}

void ble_evt_hardware_adc_result(const struct ble_msg_hardware_adc_result_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Hardware adc result event\n");
#endif
}

void ble_evt_hardware_analog_comparator_status(const struct ble_msg_hardware_analog_comparator_status_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Hardware analog comparator status event\n");
#endif
}

void ble_evt_dfu_boot(const struct ble_msg_dfu_boot_evt_t *msg)
{
#ifdef DEBUG
	UARTconsole_printf("Device Firmware Update boot event\n");
#endif
}

