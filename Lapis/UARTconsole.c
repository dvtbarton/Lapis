/*
 * UARTconsole.c
 *
 *  Created on: Nov 9, 2015
 *      Author: Todd B
 */

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "apitypes.h"
#include "UARTconsole.h"

//================================================
//  UARTconsole_Init()
//
//  @param uint32_t baud: the baud rate to set the UART
//
//  outputs: none
//
//  notes: UART0 is connected to the virtual comm port over USB
//  and the ICDI debugger.
//  To communicate with a serial console on the computer via USB
//  you must use UART0.
//  PortB PA0 is U0Rx
//  PortB PB1 is U0Tx
//================================================
void UARTconsole_Init(uint32_t baud)
{
	// UART0 is connected to the virtual serial port, thus comm to the PC serial console
	// Enable the GPIO Peripheral used by the UART
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

	// Enable UART0
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

	// Configure GPIO pins for UART0
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	// Enable debug console on UART0 at 115200 baud
	UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);
	UARTStdioConfig(0, baud, 16000000);
}

void UARTconsole_printf(const char* string, ...)
{
	UARTprintf(string);
}
