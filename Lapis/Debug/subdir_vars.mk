################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../Lapis_ccs.cmd 

C_SRCS += \
../BLE113comm.c \
../LaunchpadRGB.c \
../UARTconsole.c \
../cmd_def.c \
../commands.c \
../main.c \
../startup_ccs.c 

OBJS += \
./BLE113comm.obj \
./LaunchpadRGB.obj \
./UARTconsole.obj \
./cmd_def.obj \
./commands.obj \
./main.obj \
./startup_ccs.obj 

C_DEPS += \
./BLE113comm.pp \
./LaunchpadRGB.pp \
./UARTconsole.pp \
./cmd_def.pp \
./commands.pp \
./main.pp \
./startup_ccs.pp 

C_DEPS__QUOTED += \
"BLE113comm.pp" \
"LaunchpadRGB.pp" \
"UARTconsole.pp" \
"cmd_def.pp" \
"commands.pp" \
"main.pp" \
"startup_ccs.pp" 

OBJS__QUOTED += \
"BLE113comm.obj" \
"LaunchpadRGB.obj" \
"UARTconsole.obj" \
"cmd_def.obj" \
"commands.obj" \
"main.obj" \
"startup_ccs.obj" 

C_SRCS__QUOTED += \
"../BLE113comm.c" \
"../LaunchpadRGB.c" \
"../UARTconsole.c" \
"../cmd_def.c" \
"../commands.c" \
"../main.c" \
"../startup_ccs.c" 


