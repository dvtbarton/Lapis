/*
 * BLE113comm.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: Todd B
 */

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "apitypes.h"
#include "cmd_def.h"
#include "UARTconsole.h"
#include "BLE113comm.h"

//=======================================
// PRIVATE FUNCTION FORWARD DECLARATIONS
//=======================================
bool UART_Transmit(uint16 length, uint8* contents);
void BLE113comm_print_raw_packet(struct ble_header *hdr, unsigned char *data, uint8 outgoing);
void change_state(states new_state);
void get_payload(uint8 length, unsigned char* data);


//===============================
//  PRIVATE VARIABLES
//===============================
// friendly names for above list of states
const char *state_names[state_last] = {
    "disconnected",
    "connecting",
    "connected",
    "finding_services",
    "finding_attributes",
    "listening_measurements",
    "finish"
};

states state = state_disconnected;


//================================================
//  BLE113comm_Init()
//
//  @param uint32_t baud: the baud rate to set the UART
//
//  outputs: none
//
//  notes: UART1 is the only module on the TM4C123GH6PM
//  that has hardware flow control used to communicate
//  with the BLE113 module.
//  PortB PB0 is U1Rx
//  PortB PB1 is U1Tx
//  PortC PC4 is U1RTS
//  PortC PC5 is U1CTS
//  Configured with 8 bit length, no parity, 1 stop bit
//================================================
void BLE113comm_Init(uint32_t baud)
{
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	// Enable GPIO pins for UART1 HW flow control
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);

	// Enable UART1
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);

	// Configure GPIO pins for UART1
	ROM_GPIOPinConfigure(GPIO_PB0_U1RX);
	ROM_GPIOPinConfigure(GPIO_PB1_U1TX);
	ROM_GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	// Configure GPIO pins for UART1 HW flow control
	ROM_GPIOPinConfigure(GPIO_PC4_U1RTS);
	ROM_GPIOPinConfigure(GPIO_PC5_U1CTS);
	ROM_GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);

	// Setup UART1 at 115200 baud, 8 bits, 1 stop bit, no parity
	UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);
	UARTConfigSetExpClk(UART1_BASE, 16000000, baud, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
	// Enable RTS/CTS HW flow control for UART1
	UARTFlowControlSet(UART1_BASE, UART_FLOWCONTROL_RX | UART_FLOWCONTROL_TX);
	// Enable UART1, this call also enables the FIFO buffer necessary for HW flow control
	UARTEnable(UART1_BASE);


	// Set state to disconnected
	state = state_disconnected;

	BLE113comm_connection->primary_service_uuid[0] = 0x00;
	BLE113comm_connection->primary_service_uuid[1] = 0x28;
}



//======================================================================
//  BLE113comm_send_api_packet()
//
// Inputs: none
// Outputs: none
//
// Notes: UART protocol to send commands to the BGAPI on BLE113 module.
//
// MUST be set to bglib_output
//======================================================================
void BLE113comm_send_api_packet(uint8 len1,uint8* data1,uint16 len2,uint8* data2)
{
	// display outgoing BGAPI packet
//	BLE113comm_print_raw_packet((struct ble_header *)data1, data2, 1);

	if(UART_Transmit((uint16) len1, data1))
	{
		UART_Transmit(len2, data2);
	}
}

//=================================================
//  UART_Transmit()
// Notes: this is a "private" function to be accessed
// only within BLE113comm.c
//=================================================
bool UART_Transmit(uint16 length, uint8* contents)
{
	bool transmitSuccess = true;

	while(length>0 && transmitSuccess == true)
	{
		transmitSuccess = UARTCharPutNonBlocking(UART1_BASE, *contents);

		if(transmitSuccess)
		{
			*contents++;
			length--;
		}
		else
			break;
	}

	return true; //return transmitSuccess;
}



//=======================================
//  read_api_packet()
//=======================================
int BLE113comm_read_api_packet(int timeout_ms)
{

	struct ble_header hdr;
	unsigned char data[256] = {0};

	hdr.lolen = 0;

//	if(UARTCharsAvail(UART1_BASE))
	{
		// Read received header
		hdr.type_hilen = UARTCharGet(UART1_BASE);

		hdr.lolen = UARTCharGet(UART1_BASE);

		hdr.cls = UARTCharGet(UART1_BASE);

		hdr.command = UARTCharGet(UART1_BASE);
	}

	if(hdr.lolen)
	{
		get_payload(hdr.lolen, data);
	}

	const struct ble_msg *msg = ble_get_msg_hdr(hdr);

	if(!msg)
	{
//		UARTconsole_printf("No message\n");
		return 1;
	}
    // display incoming BGAPI packet
//    BLE113comm_print_raw_packet(&hdr, data, 0);

    // call the appropriate handler function with any payload data
    // (this is what triggers the ble_evt_* and ble_rsp_* functions)
	msg->handler(data);

	return 0;
}

void BLE113comm_print_raw_packet(struct ble_header *hdr, unsigned char *data, uint8 outgoing)
{
    int i;
    UARTconsole_printf(outgoing ? "TX => [ " : "RX <= [ ");
    for (i = 0; i < 4; i++) {   // display first 4 bytes, always present in every packet
    	UARTconsole_printf("%02X ", ((unsigned char *)hdr)[i]);
    }
    if (hdr -> lolen > 0) {
    	UARTconsole_printf("| ");           // display remaining data payload
        for (i = 0; i < hdr -> lolen; i++) {
        	UARTconsole_printf("%02X ", ((unsigned char *)hdr)[i + 4]);
        }
    }
    UARTconsole_printf("]\n");
}


/**
 * Change application state
 *
 * @param new_state New state to enter
 */
void change_state(states new_state)
{
    #ifdef DEBUG
        // show current and next state (friendly names, not IDs)
        UARTconsole_printf("DEBUG: State changed: %s --> %s\n", state_names[state], state_names[new_state]);
    #endif

    state = new_state;
}

//=====================================
//  BLE113comm_get_state()
//  Inputs: none
//  Outputs: returns the current state
//           of BLE113comm
//=====================================
states BLE113comm_get_state(void)
{
	return state;
}

//=====================================
//  BLE113comm_set_state()
//  @param states set_state - sets the state
//  of BLE113comm to the input set_state
//  Outputs: none
//=====================================
void BLE113comm_set_state(states set_state)
{
#ifdef DEBUG
	UARTconsole_printf("State change: %s --> %s\n", state_names[state], state_names[set_state]);
#endif
	state = set_state;
}


//==================================================
//	get_payload()
//	Private function
//	@param uint8 length - length of the payload
//	@param unsigned char* data - data array pointer
//	Outputs: none
//==================================================
void get_payload(uint8 length, unsigned char* data)
{
	unsigned char i = 0;

	while(length--)
	{
//		if(UARTCharsAvail(UART1_BASE))
		{
			data[i] = UARTCharGet(UART1_BASE);
			i++;
		}
	}
}


//==================================================
//	BLE113comm_errors()
//	@param uint16 error_code - code used in the switch
//	See Bluegiga Bluetooth Smart Software API Documentation
//	for full descriptions of the error codes
//	Outputs: none
//==================================================
void BLE113comm_errors(uint16 error_code)
{
	switch(error_code)
	{
	case 0: break;	// No error
	//  BGAPI errors
	case 0x0180:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol invalid parameter\n", error_code);
		break;
	case 0x0181:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol device in wrong state to receive command\n", error_code);
		break;
	case 0x0182:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol device out of memory\n", error_code);
		break;
	case 0x0183:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol feature not implemented\n", error_code);
		break;
	case 0x0184:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol command not recognized\n", error_code);
		break;
	case 0x0185:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol command or feature failed due to TIMEOUT\n", error_code);
		break;
	case 0x0186:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol not connected, connection handle passed to command is not a valid handle\n", error_code);
		break;
	case 0x0187:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol Flow, command would cause either underflow or overflow\n", error_code);
		break;
	case 0x0188:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol user attribute access through API is not supported\n", error_code);
		break;
	case 0x0189:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol invalid license key\n", error_code);
		break;
	case 0x018A:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol command too long, command maximum length exceeded\n", error_code);
		break;
	case 0x018B:
		UARTconsole_printf("ERROR (%x): BGAPI Protocol out of bonds, no space left for additional bonding\n", error_code);
		break;
	//  Bluetooth errors
	case 0x0205:
		UARTconsole_printf("ERROR (%x): Bluetooth authentication failure, incorrect results possibly due to incorrect PIN or link key\n", error_code);
		break;
	case 0x0206:
		UARTconsole_printf("ERROR (%x): Bluetooth PIN or key missing\n", error_code);
		break;
	case 0x0207:
		UARTconsole_printf("ERROR (%x): Bluetooth memory capacity exceeded, controller is out of memory\n", error_code);
		break;
	case 0x0208:
		UARTconsole_printf("ERROR (%x): Bluetooth connection timeout, link supervision timeout has expired\n", error_code);
		break;
	case 0x0209:
		UARTconsole_printf("ERROR (%x): Bluetooth connection limit exceeded, controller is at limit of connections supported\n", error_code);
		break;
	case 0x020C:
		UARTconsole_printf("ERROR (%x): Bluetooth command disallowed, command requested cannot be executed because the Controller is in a state where it cannot process this command at this time\n", error_code);
		break;
	case 0x0212:
		UARTconsole_printf("ERROR (%x): Bluetooth invalid command parameters\n", error_code);
		break;
	case 0x0213:
		UARTconsole_printf("ERROR (%x): Bluetooth remote user terminated connection\n", error_code);
		break;
	case 0x0216:
		UARTconsole_printf("ERROR (%x): Bluetooth local host terminated connection\n", error_code);
		break;
	case 0x0222:
		UARTconsole_printf("ERROR (%x): Bluetooth link layer response timeout\n", error_code);
		break;
	case 0x0228:
		UARTconsole_printf("ERROR (%x): Bluetooth link layer instant passed, Received link-layer control packet where instant was in the past\n", error_code);
		break;
	case 0x023A:
		UARTconsole_printf("ERROR (%x): Bluetooth controller busy, unable to process request\n", error_code);
		break;
	case 0x023B:
		UARTconsole_printf("ERROR (%x): Bluetooth unacceptable connection interval, remote device terminated connection\n", error_code);
		break;
	case 0x023C:
		UARTconsole_printf("ERROR (%x): Bluetooth directed advertising timeout, directed advertising completed without a connection being created\n", error_code);
		break;
	case 0x023D:
		UARTconsole_printf("ERROR (%x): Bluetooth Message Integrity Check failed on a received packet\n", error_code);
		break;
	case 0x023E:
		UARTconsole_printf("ERROR (%x): Bluetooth connection failed to be established, controller did not receive any packets from remote device\n", error_code);
		break;
	//  Security Manager Protocol errors
	case 0x0301:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Passkey entry failed\n", error_code);
		break;
	case 0x0302:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Out of Band data is not available\n", error_code);
		break;
	case 0x0303:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Authentication requirements cannot be met\n", error_code);
		break;
	case 0x0304:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Confrim value failed\n", error_code);
		break;
	case 0x0305:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Pairing not supported by device\n", error_code);
		break;
	case 0x0306:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Encryption key size insufficient\n", error_code);
		break;
	case 0x0307:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Command not supported\n", error_code);
		break;
	case 0x0308:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Unspecified Reason\n", error_code);
		break;
	case 0x0309:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Repeated attempts\n", error_code);
		break;
	case 0x030A:
		UARTconsole_printf("ERROR (%x): Security Manager Protocol Invalid parameters\n", error_code);
		break;
	//  Attribute Protocol errors
	case 0x0401:
		UARTconsole_printf("ERROR (%x): Attribute Protocol invalid handle, the attribute handle is not valid on this server\n", error_code);
		break;
	case 0x0402:
		UARTconsole_printf("ERROR (%x): Attribute Protocol read not permitted\n", error_code);
		break;
	case 0x0403:
		UARTconsole_printf("ERROR (%x): Attribute Protocol write not permitted\n", error_code);
		break;
	case 0x0404:
		UARTconsole_printf("ERROR (%x): Attribute Protocol invalid PDU\n", error_code);
		break;
	case 0x0405:
		UARTconsole_printf("ERROR (%x): Attribute Protocol innsufficient authentication, authentication required before read/write\n", error_code);
		break;
	case 0x0406:
		UARTconsole_printf("ERROR (%x): Attribute Protocol request not supported, attribute server does not support request received from the client\n", error_code);
		break;
	case 0x0407:
		UARTconsole_printf("ERROR (%x): Attribute Protocol invalid offset, offset specified is past the end of the attribute\n", error_code);
		break;
	case 0x0408:
		UARTconsole_printf("ERROR (%x): Attribute Protocol insufficient authorization, authorization required before read/write\n", error_code);
		break;
	case 0x0409:
		UARTconsole_printf("ERROR (%x): Attribute Protocol prepare queue full\n", error_code);
		break;
	case 0x040A:
		UARTconsole_printf("ERROR (%x): Attribute Protocol attribute not found within attribute handle range\n", error_code);
		break;
	case 0x040B:
		UARTconsole_printf("ERROR (%x): Attribute Protocol attribute not long, attribute cannot be read/written using Read Blob Request\n", error_code);
		break;
	case 0x040C:
		UARTconsole_printf("ERROR (%x): Attribute Protocol insufficient encryption key size\n", error_code);
		break;
	case 0x040D:
		UARTconsole_printf("ERROR (%x): Attribute Protocol invalid attribute value length\n", error_code);
		break;
	case 0x040E:
		UARTconsole_printf("ERROR (%x): Attribute Protocol unlikely error, the attribute request that was requested has encountered an error that was unlikely, and therefore could not be completed as requested\n", error_code);
		break;
	case 0x040F:
		UARTconsole_printf("ERROR (%x): Attribute Protocol insufficient encryption, encryption required before read/write\n", error_code);
		break;
	case 0x0410:
		UARTconsole_printf("ERROR (%x): Attribute Protocol unsupported group type, the attribute type is not a supported grouping attribute as defined by a higher layer specification\n", error_code);
		break;
	case 0x0411:
		UARTconsole_printf("ERROR (%x): Attribute Protocol insufficient resources to complete request\n", error_code);
		break;
	case 0x0480:
		UARTconsole_printf("ERROR (%x): Attribute Protocol application error codes, defined by higher layer specification\n", error_code);
		break;
	default:
		UARTconsole_printf("ERROR (%x): Default, unknown error\n", error_code);
	}
}


