/*
 * BLE113comm.h
 *
 *  Created on: Nov 7, 2015
 *      Author: Todd B
 */

#ifndef BLE113COMM_H
#define BLE113COMM_H

#include "apitypes.h"

// list all possible pending actions
enum actions {
    action_none,
    action_scan,
    action_connect,
    action_info,
};

typedef enum states_t
{
	state_disconnected,             // start/idle state
	state_connecting,               // connection in progress but not established
	state_connected,                // connection established
	state_finding_services,         // listing services
	state_finding_attributes,       // listing attributes
	state_listening_measurements,   // indications enabled, waiting for updates from sensor
	state_finish,                   // application process complete, will exit immediately
	state_last                      // enum tail placeholder
}states;

struct BLE113comm_connection_tag
{
	uint8 connection;			// Connection handle
	uint8 flags;					// Connection status flags use connstatus-enumerator
	bd_addr address;				// Remote devices Bluetooth address
	uint8 address_type;		// Remote address type, see: Bluetooth Address Types--gap
	uint16 conn_interval;	// Current connection interval (units of 1.25 ms)
	uint16 timeout;				// Current supervision timeout (units of 10 ms)
	uint16 latency;				// Slave latency which tells how many connection intervals the slave may skip
	uint8 bonding;				// Bonding handle if the device has been bonded with. Otherwise: 0xFF
	uint8 primary_service_uuid[2];

}*BLE113comm_connection;

struct BLE113comm_services_tag
{
	uint16 generic_service_handle_start;
	uint16 generic_service_handle_end;

}*BLE113comm_services;

struct BLE113comm_addresses_tag
{
	bd_addr local;

}*BLE113comm_addresses;

#define FIRST_HANDLE	0x0001
#define LAST_HANDLE		0xffff

#define GENERIC_ACCESS_UUID		0x1800
#define REMOTE_DEVICE_NAME		0x2A00

void BLE113comm_Init(uint32_t baud);
void BLE113comm_send_api_packet(uint8 len1,uint8* data1,uint16 len2,uint8* data2);
int BLE113comm_read_api_packet(int timeout_ms);
states BLE113comm_get_state(void);
void BLE113comm_set_state(states set_state);
void BLE113comm_errors(uint16 error_code);

#endif /* BLE113_COMM_H_ */
