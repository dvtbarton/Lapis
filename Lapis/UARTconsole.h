/*
 * UARTconsole.h
 *
 *  Created on: Nov 9, 2015
 *      Author: Todd B
 */

#ifndef UARTCONSOLE_H_
#define UARTCONSOLE_H_

#include <stdint.h>

void UARTconsole_Init(uint32_t baud);
void UARTconsole_printf(const char* string, ...);

#endif /* UARTCONSOLE_H_ */
