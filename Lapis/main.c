//*****************************************************************************
//
// hello.c - Simple hello world example.
//
// Copyright (c) 2012-2015 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
//
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 2.1.1.71 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "UARTconsole.h"
#include "BLE113comm.h"
#include "LaunchpadRGB.h"
#include "apitypes.h"
#include "cmd_def.h"

//*****************************************************************************
//
//! <h1>Lapis</h1>
//!
//! A very simple ``hello world'' example.  It simply displays ``Hello World!''
//! on the UART and is a starting point for more complicated applications.
//!
//! UART0, connected to the Virtual Serial Port and running at
//! 115,200, 8-N-1, is used to display messages from this application.
//
//*****************************************************************************

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif


uint8 primary_service_uuid[] = {0x00, 0x28};



//*****************************************************************************
//
// Main function
//
//*****************************************************************************
int main(void)
{
	uint32_t baud = 115200;
	// Set the clocking to run directly from the crystal.
		ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN);
		// Initialize debug UART
		UARTconsole_Init(baud);
		// Initialize UART for BLE113 communication
		BLE113comm_Init(baud);

		LaunchpadRGB_Init();

		// This sets the UART communication protocol to the
		// required BGLIB pointer function in order to correctly
		// send commands to the BGAPI
		bglib_output = BLE113comm_send_api_packet;

		// Reset BLE113 to get into known state
		ble_cmd_system_reset(0);	// 0 is boot in main program
		BLE113comm_read_api_packet(0);


		UARTconsole_printf("Say hello\n");
		ble_cmd_system_hello();
		BLE113comm_read_api_packet(0);


//		ble_cmd_gap_discover(gap_discover_observation);
//		BLE113comm_read_api_packet(0);

		ble_cmd_system_address_get();
		BLE113comm_read_api_packet(0);

		ble_cmd_gap_set_mode(gap_general_discoverable, gap_undirected_connectable);	// General discoverable, Undirected connectable
//		ble_cmd_gap_discover(gap_discover_generic);
		BLE113comm_read_api_packet(0);

//		ble_cmd_sm_set_bondable_mode(1);	// Set mode to bondable
//		BLE113comm_read_api_packet(0);
//
//		ble_cmd_sm_delete_bonding(0xFF);	// Delete all bonds
//		BLE113comm_read_api_packet(0);

//		ble_cmd_sm_get_bonds();
		while(1)
		{
			BLE113comm_read_api_packet(0);
//			if(BLE113comm_get_state() == state_connected)
//			{
//				ble_cmd_connection_get_rssi(BLE113comm_connection->connection);
//			}
			if(BLE113comm_get_state() == state_finding_services)
			{
				ble_cmd_attclient_read_by_group_type(BLE113comm_connection->connection, FIRST_HANDLE, LAST_HANDLE, 2, primary_service_uuid);
			}
			else if (BLE113comm_get_state() == state_finding_attributes)
			{
				ble_cmd_attclient_find_information(BLE113comm_connection->connection, BLE113comm_services->generic_service_handle_start, BLE113comm_services->generic_service_handle_end);
			}




		}
}
