// LaunchpadRGB.c
// By: Todd Barton
// July 7, 2015
// Accesses the onboard Red-Green-Blue LED on TM4C123 Launchpad
// The LED is attached to PortF pins PF1 (Red), PF2 (Blue), and PF3 (Green)

// Color    LED(s) PortF
// dark     ---    0
// red      R--    0x02
// blue     --B    0x04
// green    -G-    0x08
// yellow   RG-    0x0A
// sky blue -GB    0x0C
// white    RGB    0x0E
// pink     R-B    0x06

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "LaunchpadRGB.h"

// Define pin to LED color mapping

#define RED_LED   GPIO_PIN_1
#define BLUE_LED  GPIO_PIN_2
#define GREEN_LED GPIO_PIN_3

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

//===================================
//  LaunchpadRGB_Init()
//===================================
// PF3,PF2,PF1 are outputs to the LED
// Inputs: None
// Outputs: None
// Notes: These three pins are connected to hardware on the LaunchPad
void LaunchpadRGB_Init(void){

  //
  // Enable and configure the GPIO port for the LED operation.
  //
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED);

}


void LaunchpadRGB_Off() 
{
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, OFF);
}

void LaunchpadRGB_Red() 
{
  // Red is on PF1
  GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, RED_LED);
}

void LaunchpadRGB_Green() 
{
  // Green is on PF3
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, GREEN_LED);
}

void LaunchpadRGB_Blue() 
{
  // Blue is on PF2
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, BLUE_LED);
}

void LaunchpadRGB_Yellow() 
{
	// Yellow is RED|GREEN
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, RED_LED|GREEN_LED);
}

void LaunchpadRGB_Cyan() 
{
	// Cyan is GREEN|BLUE
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, GREEN_LED|BLUE_LED);
}

void LaunchpadRGB_Purple() 
{
	// Purple is RED|BLUE
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, RED_LED|BLUE_LED);
}

void LaunchpadRGB_White() 
{
	// White is RED|BLUE|GREEN
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, RED_LED|BLUE_LED|GREEN_LED);
}
