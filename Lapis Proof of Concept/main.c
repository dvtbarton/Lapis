//*****************************************************************************
//
// hello.c - Simple hello world example.
//
// Copyright (c) 2012-2015 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.1.71 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "LaunchpadRGB.h"
#include "apitypes.h"
#include "cmd_def.h"

//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>Hello World (hello)</h1>
//!
//! A very simple ``hello world'' example.  It simply displays ``Hello World!''
//! on the UART and is a starting point for more complicated applications.
//!
//! UART0, connected to the Virtual Serial Port and running at
//! 115,200, 8-N-1, is used to display messages from this application.
//
//*****************************************************************************

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

//*****************************************************************************
//
// Configure UART0 and UART1 pins.  This must be called before UARTprintf().
//
//*****************************************************************************
void
ConfigureUARTs(void)
{
	// Enable the GPIO Peripheral used by the UARTs.
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	// Enable GPIO pins for UART1 HW flow control
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);

	// Enable UART0
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	// Enable UART1
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);

	// Configure GPIO pins for UART0
	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	// Configure GPIO pins for UART1
	ROM_GPIOPinConfigure(GPIO_PB0_U1RX);
	ROM_GPIOPinConfigure(GPIO_PB1_U1TX);
	ROM_GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	// Configure GPIO pins for UART1 HW flow control
	ROM_GPIOPinConfigure(GPIO_PC4_U1RTS);
	ROM_GPIOPinConfigure(GPIO_PC5_U1CTS);
	ROM_GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);

	// Enable debug console on UART0 at 115200 baud
	UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);
	UARTStdioConfig(0, 115200, 16000000);
	// Setup UART1 at 115200 baud, 8 bits, 1 stop bit, no parity
	UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);
	UARTConfigSetExpClk(UART1_BASE, 16000000, 115200, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
	// Enable RTS/CTS HW flow control for UART1
	UARTFlowControlSet(UART1_BASE, UART_FLOWCONTROL_RX | UART_FLOWCONTROL_TX);
	// Enable UART1, this call also enables the FIFO buffer necessary for HW flow control
	UARTEnable(UART1_BASE);
}

//=================================================
//  UART_Transmit()
//=================================================
bool UART_Transmit(uint16 length, uint8* contents)
{
	bool transmitSuccess = true;

	while(length>0 && transmitSuccess == true)
	{
		transmitSuccess = UARTCharPutNonBlocking(UART1_BASE, *contents);
		*contents++;
		length--;
	}

	return true; //return transmitSuccess;
}

//=====================================================
// Prints a string (buf) followed by a CRLF (\r\n)
//=====================================================
//void
//UART1Println(const char *buf)
//{
//	while(*buf)
//	{
//		UARTCharPut(UART1_BASE, *buf++);
//	}
//	UARTCharPut(UART1_BASE, '\r');
//	UARTCharPut(UART1_BASE, '\n');
//}

//======================================================================
//  send_api_packet()
//======================================================================
void send_api_packet(uint8 len1,uint8* data1,uint16 len2,uint8* data2)
{
	if(UART_Transmit((uint16) len1, data1))
	{
		UART_Transmit(len2, data2);
	}
}



//=======================================
//  read_api_packet()
//=======================================
int read_api_packet(int timeout_ms)
{

	struct ble_header hdr;

	unsigned char data[256];



	// Read received header

	hdr.type_hilen = UARTCharGet(UART1_BASE);

	hdr.lolen = UARTCharGet(UART1_BASE);

	hdr.cls = UARTCharGet(UART1_BASE);

	hdr.command = UARTCharGet(UART1_BASE);



	const struct ble_msg *msg = ble_get_msg_hdr(hdr);

	if(!msg)
	{

		// This is where the code always ends up
		UARTprintf("No message\n");
		return 1;

	}

	msg->handler(data);



	return 0;

}

void ble_rsp_system_hello(const void* nul)
{
	UARTprintf("Hello!\n");
#ifdef DEBUG
	UARTprintf("DEBUG MODE\n");
#endif
}

void ble_rsp_gap_set_mode(const struct ble_msg_gap_set_mode_rsp_t *msg)
{
	UARTprintf("Mode set\n");
}


void ble_rsp_gap_discover(const struct ble_msg_gap_discover_rsp_t *msg)
{
	UARTprintf("Scanning...\n");
}


void ble_rsp_connection_get_status(const struct ble_msg_connection_get_status_rsp_t *msg)
{
	UARTprintf("Getting connection status...\n");

}

void ble_evt_connection_status(const struct ble_msg_connection_status_evt_t *msg)
{
	uint8 connection = msg->connection;			// Connection handle
	uint8 flags = msg->flags;					// Connection status flags use connstatus-enumerator
	bd_addr address = msg->address;				// Remote devices Bluetooth address
	uint8 address_type = msg->address_type;		// Remote address type, see: Bluetooth Address Types--gap
	uint16 conn_interval = msg->conn_interval;	// Current connection interval (units of 1.25 ms)
	uint16 timeout = msg->timeout;				// Current supervision timeout (units of 10 ms)
	uint16 latency = msg->latency;				// Slave latency which tells how many connection intervals the slave may skip
	uint8 bonding = msg->bonding;				// Bonding handle if the device has been bonded with. Otherwise: 0xFF

	UARTprintf(connection);


}

void ble_rsp_system_get_info(const struct ble_msg_system_get_info_rsp_t *msg)
{
}
//*****************************************************************************
//
// Print "Hello World!" to the UART on the evaluation board.
//
//*****************************************************************************
int
main(void)
{
	// Set the clocking to run directly from the crystal.
		ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN);
		// Initialize the UARTs
		ConfigureUARTs();
		LaunchpadRGB_Init();
		bglib_output = send_api_packet;

		UARTprintf("Say hello\n");
		ble_cmd_system_hello();
		read_api_packet(0);



//		ble_cmd_gap_discover(gap_discover_observation);
//		read_api_packet(0);

		// software reset
		// enable security mode 1 with fixed pin '1234'
		// set device friendly name to 'dashboard'
		// make device discoverable

		//UARTprintf("Enabled service discovery\n");

		// register local service with serial port profile
		// enable auto accepting connection requests

		//UARTprintf("Auto accepting connection requests...\n");
		LaunchpadRGB_White();

		// wait for a device to connect and then enter streaming mode


//		UARTprintf("Received connection request\n");
//		UARTprintf("Entering streaming mode...\n");

		// request streaming mode

		// wait for OK

//		UARTprintf("Entered streaming mode\n\n");
		ble_cmd_gap_set_mode(2,2);	// General discoverable, Undirected connectable
		// loop and echo back incoming characters to the bluetooth serial as well as the debug console
		while(1) {
			read_api_packet(0);
//			char c = UARTCharGet(UART1_BASE);
//			UARTprintf("%c", c);
//			UARTCharPut(UART1_BASE, c);
		}
}
