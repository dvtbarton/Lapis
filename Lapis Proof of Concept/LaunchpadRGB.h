// LaunchpadRGB.h
// By: Todd Barton
// July 7, 2015
// Accesses the onboard Red-Green-Blue LED on TM4C123 Launchpad
// The LED is attached to PortF pins PF1 (Red), PF2 (Blue), and PF3 (Green)

#ifndef LAUNCHPADRGB_H
#define LAUNCHPADRGB_H

#define ON 	1
#define OFF 0

void LaunchpadRGB_Init(void);
void LaunchpadRGB_Off(void);
void LaunchpadRGB_Red(void);
void LaunchpadRGB_Green(void);
void LaunchpadRGB_Blue(void);
void LaunchpadRGB_Yellow(void);
void LaunchpadRGB_Cyan(void);
void LaunchpadRGB_Purple(void);
void LaunchpadRGB_White(void);

#endif
