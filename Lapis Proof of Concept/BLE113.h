/*
 * BLE113.h
 *
 *  Created on: Nov 7, 2015
 *      Author: Todd B
 */

#ifndef BLE113_H_
#define BLE113_H_

// list all possible pending actions
enum actions {
    action_none,
    action_scan,
    action_connect,
    action_info,
};

typedef enum states_t
{
	state_disconnected,             // start/idle state
	state_connecting,               // connection in progress but not established
	state_connected,                // connection established
	state_finding_services,         // listing services (searching for HTM service)
	state_finding_attributes,       // listing attributes (searching for HTM measurement characteristic)
	state_listening_measurements,   // indications enabled, waiting for updates from sensor
	state_finish,                   // application process complete, will exit immediately
	state_last                      // enum tail placeholder
}states;



class BLE113 {
public:
	BLE113();
	virtual ~BLE113();
};

#endif /* BLE113_H_ */
